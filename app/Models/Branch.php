<?php

namespace RMS\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    // use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'address',
    	'email',
    	'contactNo',
    	'resturent_id'
    ];

    public function resturent() {
        return $this->belongsTo('RMS\Models\Resturent');
    }

    public function departments(){
        return $this->belongsToMany('RMS\Models\Department')->withTimestamps();
    }
    
    public function locations() {
        return $this->hasMany('RMS\Models\Location');
    }
}
