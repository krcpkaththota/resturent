<?php

namespace RMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use RMS\Models\Resturent;
use RMS\Models\Branch;
use RMS\Models\Department;
use RMS\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder;
class BranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $branches = Branch::all();
        

        $params = [
            'title' => 'Branches List',
            'branches' => $branches,
        ];
        return view('admin.branches.branches_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $resturents = Resturent::all();
        $departments = Department::all();
        $params = [
            'title' => 'Create Branch',
            'resturents' => $resturents,
            'departments' => $departments,

        ];
        return view('admin.branches.branches_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // var_dump($request->input('departments'));
        $contactNoString = implode(",", $request->input('contactNo'));
        $departments = $request->input('departments');        

         $branch = Branch::create([
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'email' => $request->input('email'),
            'contactNo' => $contactNoString,
            'resturent_id' => $request->input('resturent_id'),
        ]);

        $branch->departments()->attach($departments);

        return redirect()->route('branches.index')->with('success', "The branch <strong>$branch->name</strong> has successfully been created.
            ");
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try{
            $branch = Branch::findOrFail($id);

            $params = [
                'title' => 'Delete Branch',
                'branch' => $branch,
            ];

            return view('admin.branches.branches_delete')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try
        {
            $branch = Branch::findOrFail($id);
            $resturents = Resturent::all();
            $departments = Department::all();


            $params = [
                'title' => 'Edit Branch',
                'branch' => $branch,
                'departments' => $departments,
                'resturents' => $resturents,
            ];

            return view('admin.branches.branches_edit')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $branch = Branch::findOrFail($id);

            $contactNoString = implode(",", array_filter($request->post('contactNo'),function($val){
                return $val && $val != '';
            }));

            $departments = $request->input('departments');


            $this->validate($request, [
                'name' => 'required',
                'address' => 'required',
                'email' => 'required|unique:branches,email,'.$id,
                'contactNo' => 'required',
                'resturent_id' => 'required',
            ]);

            $branch->name = $request->input('name');
            $branch->address = $request->input('address');
            $branch->email = $request->input('email');
            $branch->contactNo = $contactNoString;
            $branch->resturent_id = $request->input('resturent_id');

            $branch->save();
            $branch->departments();
            $branch->departments()->sync($departments);

            return redirect()->route('branches.index')->with('success', "The branch <strong>$branch->name</strong> has successfully been updated.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {
            $branch = Branch::findOrFail($id);

            $branch->delete();

            return redirect()->route('branches.index')->with('success', "The branch <strong>{{$branch->name}}</strong> has successfully been archived.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
