<?php

namespace RMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use RMS\Models\Resturent;
use RMS\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ResturentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $resturents = Resturent::all();
        $params = [
            'title' => 'Resturent List',
            'resturents' => $resturents,
        ];
        return view('admin.resturents.resturents_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $params = [
            'title' => 'Create Resturent',
        ];
        return view('admin.resturents.resturents_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $resturent = Resturent::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ]);

        return redirect()->route('resturents.index')->with('success', "The resturent <strong>$resturent->name</strong> has successfully been created.
            ");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try
        {
            $resturent = Resturent::findOrFail($id);

            $params = [
                'title' => 'Delete Resturent',
                'resturent' => $resturent,
            ];

            return view('admin.resturents.resturents_delete')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try{
            $resturent = Resturent::findOrFail($id);
            $params = [
                'title' => 'Edit Resturent',
                'resturent' => $resturent,
            ];
            return view('admin.resturents.resturents_edit')->with($params);
        }catch(ModelNotFoundException $ex){

        }
        $params = [
            'title' => 'Edit Resturent',
        ];
        return view('admin.resturents.resturents_edit')->with($params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        try
        {
            $resturent = Resturent::findOrFail($id);

            $this->validate($request, [
                'name' => 'required',
            ]);

            $resturent->name = $request->input('name');
            $resturent->description = $request->input('description');

            $resturent->save();

            return redirect()->route('resturents.index')->with('success', "The resturent <strong>$resturent->name</strong> has successfully been updated.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {
            $resturent = Resturent::findOrFail($id);

            $resturent->delete();

            return redirect()->route('resturents.index')->with('success', "The resturent <strong>$resturent->name</strong> has successfully been archived.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
